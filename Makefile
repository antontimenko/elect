BASE_APPS=nginx api frontend ssr django

_TRAP=trap 'docker-compose down; exit 0' EXIT &&

all: build run

build:
	docker-compose build $(BASE_APPS)

build_all:
	docker-compose build

run:
	$(_TRAP) docker-compose up $(BASE_APPS)

shell:
	docker-compose run --rm managepy shell

makemigrations:
	docker-compose run --rm managepy makemigrations

migrate:
	docker-compose run --rm managepy migrate

down:
	docker-compose down

flake8:
	docker-compose run --rm flake8

mypy:
	docker-compose run --rm mypy

eslint:
	docker-compose run --rm eslint

lint: flake8 mypy eslint
