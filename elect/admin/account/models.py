import typing as t

from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.db import models


class UserManager(BaseUserManager):

    def create_user(
        self,
        username: str,
        password: t.Optional[str] = None,
        **extra_fields: t.Any,
    ) -> 'User':
        if not username:
            raise ValueError('The given username must be set')

        username = self.model.normalize_username(username)
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(
        self,
        username: str,
        password: t.Optional[str] = None,
        **extra_fields: t.Any,
    ) -> 'User':
        extra_fields['is_staff'] = True
        extra_fields['is_superuser'] = True

        return self.create_user(username, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):

    objects = UserManager()

    username = models.CharField(max_length=32, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'username'

    class Meta:
        db_table = 'user'
