from django.contrib import admin

from elect.admin.chatbot.models import TelegramUser


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'first_name', 'last_name', 'username', 'join_date',
        'last_update_date', 'user',
    )
