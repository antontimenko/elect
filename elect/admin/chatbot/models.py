from django.db import models


class TelegramUser(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.TextField()
    last_name = models.TextField(null=True, blank=True)
    username = models.CharField(
        max_length=32,
        unique=True,
        null=True,
        blank=True,
    )
    join_date = models.DateTimeField()
    last_update_date = models.DateTimeField()
    user = models.OneToOneField('account.User', on_delete=models.CASCADE)

    class Meta:
        db_table = 'telegram_user'
