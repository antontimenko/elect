from elect.config import config


SECRET_KEY = 'ejtd3^34r+#t*@@#wjd$aw54o$6rs$z*=5)h+lajxpe2#$^qso'

DEBUG = config['debug']

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'elect.admin.account',
    'elect.admin.chatbot',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'elect.admin.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'elect.admin.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config['postgres']['db'],
        'USER': config['postgres']['user'],
        'PASSWORD': config['postgres']['pass'],
        'HOST': config['postgres']['host'],
        'PORT': config['postgres']['port'],
    }
}

AUTH_USER_MODEL = 'account.User'

SESSION_COOKIE_AGE = config['session']['age']
SESSION_COOKIE_NAME = config['session']['cookie_name']
SESSION_ENGINE = 'elect.admin.utils.redis'

LANGUAGE_CODE = 'en-us'
TIME_ZONE = config['timezone']
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = '/static/'
