import typing as t

from django.contrib.sessions.backends.base import SessionBase
from redis import Redis

from elect.config import config
from elect.utils import (
    decode_session_data,
    encode_session_data,
    get_random_string,
    get_redis_session_key,
)


redis_client = Redis.from_url(config['redis'])


class SessionStore(SessionBase):

    def create(self) -> None:
        self._session_key: t.Optional[str] = get_random_string(32)
        self.save(must_create=True)
        self.modified = True

    def save(self, must_create: bool = True) -> None:
        if self.session_key is None:
            self.create()
            return

        redis_client.set(
            get_redis_session_key(self.session_key),
            encode_session_data(self._get_session(no_load=must_create)),
            self.get_expiry_age(),
        )

    def load(self) -> t.Dict[str, t.Any]:
        session_data = redis_client.get(get_redis_session_key(self.session_key))

        if session_data is None:
            self._session_key = None
            return {}

        return decode_session_data(session_data)

    def delete(self, session_key: t.Optional[str] = None) -> None:
        if session_key is None:
            if self.session_key is None:
                return
            session_key = self.session_key

        redis_client.delete(get_redis_session_key(self.session_key))

    def exists(self, session_key: str) -> bool:
        return redis_client.exists(get_redis_session_key(self.session_key))
