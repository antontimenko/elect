import asyncio
import typing as t

import aioredis
import uvloop
from aiohttp import web
from aiopg.sa import create_engine

from elect.apps.account.routing import routes as account_routes
from elect.apps.chatbot.routing import routes as chatbot_routes
from elect.apps.graphql.routing import routes as graphql_routes
from elect.config import config
from elect.utils import (
    session_middleware,
    user_middleware,
)


def setup_routes(app: web.Application) -> None:
    app.add_routes(account_routes)
    app.add_routes(chatbot_routes)
    app.add_routes(graphql_routes)


async def setup_db(app: web.Application) -> t.AsyncGenerator[None, None]:
    app['db'] = await create_engine(
        host=config['postgres']['host'],
        port=config['postgres']['port'],
        database=config['postgres']['db'],
        user=config['postgres']['user'],
        password=config['postgres']['pass'],
    )

    yield

    app['db'].close()
    await app['db'].wait_closed()


async def setup_redis(app: web.Application) -> t.AsyncGenerator[None, None]:
    app['redis'] = await aioredis.create_redis_pool(config['redis'])

    yield

    app['redis'].close()
    await app['redis'].wait_closed()


loop = uvloop.new_event_loop()
asyncio.set_event_loop(loop)

app = web.Application(
    debug=config['debug'],
    middlewares=[
        session_middleware,
        user_middleware,
    ],
)
setup_routes(app)
app.cleanup_ctx.append(setup_db)
app.cleanup_ctx.append(setup_redis)


def run() -> None:
    web.run_app(app, port=8002)
