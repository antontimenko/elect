from aiohttp import web

import elect.apps.account.views as views


routes = [
    web.get('/api/user', views.user_view, name='user'),
]
