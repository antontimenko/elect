import sqlalchemy as sa

from elect.db import sa_metadata


users_table = sa.Table(
    'user',
    sa_metadata,

    sa.Column('id', sa.Integer, primary_key=True),

    sa.Column('password', sa.Text, default='', nullable=False),

    sa.Column('username', sa.String(length=32), unique=True, nullable=False),
    sa.Column('is_active', sa.Boolean, default=True, nullable=False),
    sa.Column('is_staff', sa.Boolean, default=False, nullable=False),
    sa.Column('is_superuser', sa.Boolean, default=False, nullable=False),
)
