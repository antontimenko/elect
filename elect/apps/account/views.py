from aiohttp import web


async def user_view(request: web.Request) -> web.Response:
    user_dict = None
    if request['user'] is not None:
        user_dict = {
            'id': request['user'].id,
            'username': request['user'].username,
        }

    return web.json_response({'user': user_dict})
