import typing as t
from abc import (
    ABC,
    abstractmethod,
)

from aiopg.sa import SAConnection

from elect.utils.types import JsonType


class BaseChatBot(ABC):

    @abstractmethod
    async def set_webhook(self, base_url: str) -> JsonType:
        ...

    @abstractmethod
    async def get_user(
        self,
        conn: SAConnection,
        telegram_user_id: int,
        first_name: str,
        last_name: t.Optional[str],
        username: t.Optional[str],
    ) -> t.Any:
        ...
