from aiohttp import web

import elect.apps.chatbot.views as views


routes = [
    web.post(
        '/api/telegram/webhook',
        views.telegram_webhook_view,
        name='telegram_webhook',
    ),
    web.post(
        '/api/telegram/set-webhook',
        views.telegram_set_webhook_view,
        name='telegram_set_webhook',
    ),
]
