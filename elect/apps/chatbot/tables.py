import sqlalchemy as sa

from elect.db import sa_metadata


telegram_users_table = sa.Table(
    'telegram_user',
    sa_metadata,

    sa.Column('id', sa.Integer, primary_key=True),

    sa.Column('first_name', sa.Text, nullable=False),
    sa.Column('last_name', sa.Text, nullable=True),
    sa.Column('username', sa.String(length=32), unique=True, nullable=True),
    sa.Column('join_date', sa.TIMESTAMP(timezone=True)),
    sa.Column('last_update_date', sa.TIMESTAMP(timezone=True)),
    sa.Column(
        'user_id',
        sa.Integer,
        sa.ForeignKey('user'),
        unique=True,
        nullable=False,
    )
)
