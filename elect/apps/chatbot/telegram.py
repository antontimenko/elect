import typing as t
from datetime import timedelta

import aiohttp
from aiopg.sa import SAConnection
from aiopg.sa.result import RowProxy

from elect.apps.account.tables import users_table
from elect.apps.chatbot.base import BaseChatBot
from elect.apps.chatbot.tables import telegram_users_table
from elect.config import config
from elect.utils.timezone import now
from elect.utils.types import JsonType


class TelegramBot(BaseChatBot):

    def __init__(self, token: str) -> None:
        self.token = token

    async def perform_request(
        self,
        method: str,
        **params: str,
    ) -> JsonType:
        api_url = f'https://api.telegram.org/bot{self.token}/{method}'

        async with aiohttp.ClientSession() as session:
            async with session.post(api_url, data=params) as resp:
                return await resp.json()

    async def set_webhook(self, base_url: str) -> JsonType:
        url = f'{base_url}/api/telegram/webhook'
        return await self.perform_request('setWebhook', url=url)

    async def get_user(
        self,
        conn: SAConnection,
        telegram_user_id: int,
        first_name: str,
        last_name: t.Optional[str],
        username: t.Optional[str],
    ) -> RowProxy:
        now_datetime = now()

        res = await conn.execute(
            telegram_users_table
            .select()
            .where(telegram_users_table.c.id == telegram_user_id)
        )
        telegram_user = await res.fetchone()
        if telegram_user is not None:
            if (
                now_datetime
                - telegram_user.last_update_date
                > timedelta(minutes=5)
            ):
                await conn.execute(
                    telegram_users_table
                    .update()
                    .where(telegram_users_table.c.id == telegram_user_id)
                    .values(last_update_date=now_datetime)
                )

            return telegram_user

        res = await conn.execute(
            users_table
            .select()
            .where(users_table.c.username == username)
        )
        user = await res.fetchone()
        if user is None:
            user_username = username if username else f'id{telegram_user_id}'

            res = await conn.execute(
                users_table
                .insert()
                .values(username=user_username)
            )
            user_id = await res.scalar()
        else:
            user_id = user.id

        await conn.execute(
            telegram_users_table
            .insert()
            .values(
                id=telegram_user_id,
                first_name=first_name,
                last_name=last_name,
                username=username,
                join_date=now_datetime,
                last_update_date=now_datetime,
                user_id=user_id,
            )
        )

        res = await conn.execute(
            telegram_users_table
            .select()
            .where(telegram_users_table.c.id == telegram_user_id)
        )
        return await res.fetchone()


telegram_bot = TelegramBot(config['telegram_bot_token'])
