from aiohttp import web

from elect.apps.chatbot.telegram import telegram_bot


async def telegram_webhook_view(request: web.Request) -> web.Response:
    db = request.app['db']

    webhook_data = await request.json()
    if 'message' in webhook_data:
        telegram_id = webhook_data['message']['chat']['id']
        first_name = webhook_data['message']['chat']['first_name']
        last_name = webhook_data['message']['chat'].get('last_name')
        username = webhook_data['message']['chat'].get('username')

        async with db.acquire() as conn:
            user = await telegram_bot.get_user(
                conn,
                telegram_id,
                first_name,
                last_name,
                username,
            )

            print(user)

    return web.json_response({'result': 'ok'})


async def telegram_set_webhook_view(request: web.Request) -> web.Response:
    post_data = await request.post()
    await telegram_bot.set_webhook(post_data['base_url'])
    return web.json_response({'result': 'ok'})
