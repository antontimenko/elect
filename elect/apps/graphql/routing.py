from aiohttp import web

import elect.apps.graphql.views as views


routes = [
    web.view('/api/graphql', views.graphql_view, name='graphql'),
]
