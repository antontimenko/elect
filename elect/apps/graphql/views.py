from aiohttp_graphql import GraphQLView
from graphql.execution.executors.asyncio import AsyncioExecutor

from elect.apps.graphql.schema import schema


graphql_view = GraphQLView(
    schema=schema,
    graphiql=True,
    executor=AsyncioExecutor(),
)
