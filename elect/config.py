import os
import typing as t
from types import MappingProxyType

import trafaret as T
from trafaret_config import read_and_validate


POSTGRES_TRAFARET = T.Dict({
    'host': T.String(),
    'port': T.Int(),
    'db': T.String(),
    'user': T.String(),
    'pass': T.String(),
})

SESSION_TRAFARET = T.Dict({
    'age': T.Int(),
    'cookie_name': T.String(),
    'redis_prefix': T.String(),
})

TRAFARET = T.Dict({
    'debug': T.Bool(),
    'timezone': T.String(),
    'postgres': POSTGRES_TRAFARET,
    'redis': T.String(),
    'session': SESSION_TRAFARET,
    'telegram_bot_token': T.String(),
})


def immutable_dict(
    data: t.Dict[t.Hashable, t.Any],
) -> t.Mapping[t.Hashable, t.Any]:
    return MappingProxyType({
        key: immutable_dict(value) if isinstance(value, dict) else value
        for key, value in data.items()
    })


config = immutable_dict(read_and_validate(
    os.path.normpath(os.path.join('.', os.environ['CONFIG_FILE'])),
    TRAFARET,
))
