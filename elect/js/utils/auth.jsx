import React from 'react';
import { Redirect } from 'react-router-dom';

import { useAjax } from 'ajax-hooks';

const loginRequired = Component => (props) => {
    const { response } = useAjax('/api/user');

    if (!response) {
        return null;
    }

    if (!response.data.user) {
        return (
            <Redirect to='/login' />
        );
    }

    return (
        <Component user={response.data.user} {...props} />
    );
};

const anonRequired = Component => (props) => {
    const { response } = useAjax('/api/user');

    if (!response) {
        return null;
    }

    if (response.data.user) {
        return (
            <Redirect to='/' />
        );
    }

    return (
        <Component {...props} />
    );
};

export {
    loginRequired,
    anonRequired,
};
