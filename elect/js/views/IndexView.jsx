import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import qs from 'qs';

import { useAjax } from 'ajax-hooks';

import { loginRequired } from 'utils/auth';

const IndexView = ({ user }) => {
    const [baseUrl, setBaseUrl] = useState('');

    const { loading, error, response, execute } = useAjax({
        url: '/api/telegram/set-webhook',
        method: 'post',
        data: qs.stringify({ base_url: baseUrl }),
    }, {
        manual: true,
    });

    const submitButtonActive = !(loading || error || response);
    let submitButtonText = 'Set Webhook';
    if (loading) {
        submitButtonText = 'Loading';
    } else if (error) {
        submitButtonText = 'Error';
    } else if (response) {
        submitButtonText = 'Done';
    }

    const onSubmit = (e) => {
        execute()();
        e.preventDefault();
    };

    return (
        <>
            <Helmet>
                <title>Index View</title>
            </Helmet>
            <h1>Index View</h1>
            <p>
                User:
                {user.username}
            </p>
            <form onSubmit={onSubmit}>
                Webhook base url:
                <input
                    type='text'
                    value={baseUrl}
                    onChange={e => setBaseUrl(e.target.value)}
                />
                <input
                    type='submit'
                    value={submitButtonText}
                    disabled={!submitButtonActive}
                />
            </form>
        </>
    );
};

IndexView.propTypes = {
    user: PropTypes.shape({
        id: PropTypes.number.isRequired,
        username: PropTypes.string.isRequired,
    }).isRequired,
};

export default loginRequired(IndexView);
