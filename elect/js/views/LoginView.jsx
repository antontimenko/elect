import React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';

import { anonRequired } from 'utils/auth';

const LoginView = () => (
    <>
        <Helmet>
            <title>Login View</title>
        </Helmet>
        <h1>Login View</h1>
        <Link to='/'>Index View</Link>
    </>
);

export default anonRequired(LoginView);
