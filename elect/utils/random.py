import random
import string


VALID_CHARS = string.ascii_letters + string.digits


def get_random_string(length: int) -> str:
    return ''.join(random.choice(VALID_CHARS) for i in range(length))
