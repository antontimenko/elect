import json
import typing as t

from aiohttp import web

from elect.apps.account.tables import users_table
from elect.config import config


def get_redis_session_key(session_key: str) -> str:
    redis_prefix = config['session']['redis_prefix']
    return f'{redis_prefix}:{session_key}'


def encode_session_data(session_data: t.Dict[str, t.Any]) -> bytes:
    return json.dumps(session_data).encode()


def decode_session_data(raw_session_data: bytes) -> t.Dict[str, t.Any]:
    return json.loads(raw_session_data.decode())


@web.middleware
async def session_middleware(
    request: web.Request,
    handler: t.Callable[[web.Request], t.Awaitable[web.StreamResponse]],
) -> web.StreamResponse:
    redis = request.app['redis']

    session_key = request.cookies.get(config['session']['cookie_name'])

    raw_session_data = None
    if session_key is not None:
        raw_session_data = await redis.get(get_redis_session_key(session_key))

    session_data = (
        decode_session_data(raw_session_data)
        if raw_session_data is not None
        else {}
    )

    request['session'] = session_data

    return await handler(request)


@web.middleware
async def user_middleware(
    request: web.Request,
    handler: t.Callable[[web.Request], t.Awaitable[web.StreamResponse]],
) -> web.StreamResponse:
    db = request.app['db']

    user_id = request['session'].get('_auth_user_id')
    if user_id is not None:
        async with db.acquire() as conn:
            res = await conn.execute(
                users_table
                .select()
                .where(users_table.c.id == int(user_id))
            )

            request['user'] = await res.fetchone()
    else:
        request['user'] = None

    return await handler(request)
