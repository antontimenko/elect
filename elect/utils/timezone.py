from datetime import datetime

from pytz import timezone

from elect.config import config


tz = timezone(config['timezone'])


def now() -> datetime:
    return datetime.now(tz)
